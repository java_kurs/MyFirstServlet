package Cookies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by T420 on 2017-09-14.
 */
@WebServlet("/cookie_reader")
public class CookieReaderServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pW = response.getWriter();
        Cookie ck[] = request.getCookies();
        for (Cookie cookie : ck) {
            pW.println("Cookie: " + cookie.getName() + ", " + cookie.getValue());
        }
    }
}
