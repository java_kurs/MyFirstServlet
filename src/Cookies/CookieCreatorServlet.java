package Cookies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by T420 on 2017-09-14.
 */
@WebServlet("/cookie_creator")
public class CookieCreatorServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pW = response.getWriter();
        String name = request.getParameter("username");
        pW.println("Welcome " + name);
        Cookie ck = new Cookie("uname", name);
        response.addCookie(ck);
    }
}
