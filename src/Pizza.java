/**
 * Created by T420 on 2017-09-13.
 */
public class Pizza {
    String name;
    String price;

    public Pizza(String name, String price) {
        this.name = name;
        this.price = price;
    }

    public String getpName() {
        return name;
    }

    public String getpPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
