import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by T420 on 2017-09-14.
 */
@WebServlet("/summary")
public class SummaryServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pW = response.getWriter();
        pW.println("<h1>Stan sesji</h1>");
        HttpSession mySession = request.getSession(false);
        pW.println(mySession.getCreationTime());
        pW.println(mySession.getId());
        pW.println(mySession.getLastAccessedTime());
        pW.println((String)mySession.getAttribute("username"));
    }
}
