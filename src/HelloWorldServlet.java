import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by T420 on 2017-09-13.
 */
@WebServlet("/hi")
public class HelloWorldServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pW = response.getWriter();
        pW.print("<h1>dziala czy nie dziala</h1>");
        pW.print("<a href=\"http://www.google.pl\">Hello world</a>");
    }

    @Override
    public void init(){
        try {
            super.init();
            System.out.println("init");
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy(){
        System.out.println("destroy");
        super.destroy();
    }

}
