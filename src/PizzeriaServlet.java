import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by T420 on 2017-09-13.
 */
@WebServlet("/pizzeria")
public class PizzeriaServlet extends HttpServlet {
    private List<Pizza> list;
    private int id;
    @Override
    public void init(){
        try {
            super.init();
            list = new ArrayList<>();
            id=0;
            list.add(0,new Pizza("margarita","10"));
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        list.add(id,new Pizza(request.getParameter("name"), request.getParameter("price")));

    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pW = response.getWriter();
        for (int i = 0; i <list.size() ; i++) {
            pW.println(list.get(i).toString());
        }
    }
    @Override
    public void destroy(){
        list.clear();
        if(list.isEmpty()) System.out.println("list cleared");
        super.destroy();
    }
}
