package shop;

/**
 * Created by T420 on 2017-09-14.
 */
public class Item {
    private String name;
    private String price;
    private String quantity;

    public Item(String name, String price, String quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }
}
