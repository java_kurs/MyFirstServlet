import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by T420 on 2017-09-13.
 */
@WebServlet("/execution")
public class ExecutionServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pW = response.getWriter();
        pW.println("Loaded name: ");
        pW.println(request.getParameter("name"));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pW = response.getWriter();
        String name = request.getParameter("name");
        pW.println("Hello " + name);
    }
}
